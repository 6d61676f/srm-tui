use std::path::PathBuf;

use clap::{Arg, ArgAction, ArgMatches, Command};
use comfy_table::Table;
use srm_core::BackHandle;
use time::{format_description, OffsetDateTime, UtcOffset};

use super::error::Result;

pub fn get_args() -> ArgMatches {
    Command::new("safe rm")
        .version(env!("CARGO_PKG_VERSION"))
        .about("making `rm` obsolete")
        .arg(
            Arg::new("tui")
                .long("tui")
                .help("Start TUI mode")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("INPUT")
                .action(ArgAction::Append)
                .help("default -- backup args"),
        )
        .arg(
            Arg::new("yaml")
                .long("yaml")
                .action(ArgAction::SetTrue)
                .help("Enable Yaml format for I/O -- TODO input"),
        )
        .arg(
            Arg::new("json")
                .long("json")
                .action(ArgAction::SetTrue)
                .help("Enable Json format for I/O -- TODO input"),
        )
        .arg(
            Arg::new("backup")
                .long("backup")
                .help("paths to backup")
                .value_name("paths delimited by,")
                .action(ArgAction::Append),
        )
        .arg(
            Arg::new("list")
                .long("list")
                .help("List backups")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("erase")
                .long("erase")
                .use_value_delimiter(true)
                .value_name("ids delimited by,")
                .action(ArgAction::Append)
                .help("items to completely erase -- be careful"),
        )
        .arg(
            Arg::new("restore")
                .long("restore")
                .use_value_delimiter(true)
                .value_name("ids delimited by,")
                .help("items to restore")
                .action(ArgAction::Append),
        )
        .arg(
            Arg::new("compat-r")
                .short('r')
                .hide(true)
                .action(ArgAction::Count),
        )
        .arg(
            Arg::new("compat-R")
                .short('R')
                .hide(true)
                .action(ArgAction::Count),
        )
        .arg(
            Arg::new("compat-f")
                .short('f')
                .hide(true)
                .action(ArgAction::Count),
        )
        .arg(
            Arg::new("compat-i")
                .short('i')
                .hide(true)
                .action(ArgAction::Count),
        )
        .arg(
            Arg::new("compat-v")
                .short('v')
                .hide(true)
                .action(ArgAction::Count),
        )
        .get_matches()
}

pub fn run(args: ArgMatches) -> Result<()> {
    let mut srm = srm_core::SafeRm::new();

    let json = args.get_flag("json");
    let yaml = args.get_flag("yaml");

    // backup
    if let Some(to_back) = args
        .get_many("INPUT")
        .map(|vals| vals.cloned().collect::<Vec<String>>())
    {
        srm.backup(&to_back)
            .into_iter()
            .zip(to_back)
            .for_each(|(err, path)| match err {
                Ok(_) => debug!("succesfully backed up {}", path),
                Err(e) => error!("failed backing up {} {:?}", path, e),
            });
    }

    // 1. restore
    if let Some(to_restore) = args.get_many("restore") {
        let ids: Vec<BackHandle> = to_restore
            .into_iter()
            .map(|val: &String| val.parse::<BackHandle>())
            .filter_map(|val| val.ok())
            .collect();

        srm.restore_by_handle(&ids)
            .into_iter()
            .zip(ids)
            .for_each(|(err, id)| match err {
                Ok(_) => debug!("succesfully restores {}", id),
                Err(e) => error!("failed restoring {} {:?}", id, e),
            });
    };

    // 2. backup
    if let Some(to_back) = args
        .get_many("backup")
        .map(|vals| vals.cloned().collect::<Vec<PathBuf>>())
    {
        srm.backup(&to_back)
            .into_iter()
            .zip(to_back)
            .for_each(|(err, path)| match err {
                Ok(_) => debug!("succesfully backed up {}", path.to_string_lossy()),
                Err(e) => error!("failed backing up {} {:?}", path.to_string_lossy(), e),
            });
    };

    // 3. erase
    if let Some(to_erase) = args.get_many("erase") {
        let ids: Vec<BackHandle> = to_erase
            .into_iter()
            .map(|val: &String| val.parse::<BackHandle>())
            .filter_map(|val| val.ok())
            .collect();

        srm.erase_by_handle(&ids)
            .into_iter()
            .zip(ids)
            .for_each(|(err, id)| match err {
                Ok(_) => debug!("succesfully erased {}", id),
                Err(e) => error!("failed erasing {:?} {:?}", id, e),
            });
    };

    // 4. list
    if args.get_flag("list") {
        let items = srm.list_backups()?;
        let srzd = if items.is_empty() {
            String::new()
        } else if yaml {
            serde_yaml::to_string(&items)?
        } else if json {
            serde_json::to_string_pretty(&items)?
        } else {
            let mut table = Table::new();
            table.set_header(vec!["Handle", "Deletion Time", "Original Path"]);
            let fmt_dsc = format_description::parse(
                "[hour]:[minute]:[second]:[subsecond] [day]:[month]:[year]",
            )?;
            for i in items {
                // this is currently UTC TODO -- still prints UTC
                let offset = UtcOffset::current_local_offset().unwrap_or(UtcOffset::UTC);
                let dt = OffsetDateTime::from(i.id.del_time).replace_offset(offset);

                table.add_row(vec![
                    i.get_handle().to_string(),
                    dt.format(&fmt_dsc)?,
                    i.id.orig_path.to_string_lossy().to_string(),
                ]);
            }
            table.to_string()
        };

        println!("{}", srzd);
    }

    Ok(())
}
