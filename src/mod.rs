pub mod cli;
pub mod error;
pub mod events;
pub mod tui;

use clap::{App, Arg, ArgMatches};

pub fn get_args() -> ArgMatches<'static> {
    App::new("safe rm")
        .version("0.1")
        .about("making `rm` obsolete")
        .arg(Arg::with_name("tui").long("tui").help("Start TUI mode"))
        .arg(
            Arg::with_name("yaml")
                .long("yaml")
                .help("Enable Yaml format for I/O"),
        )
        .arg(
            Arg::with_name("backup")
                .long("backup")
                .help("Paths to backup")
                .value_name("paths delimited by,")
                .multiple(true)
                .use_delimiter(true)
                .takes_value(true),
        )
        .arg(Arg::with_name("list").long("list").help("List backups"))
        .arg(
            Arg::with_name("erase")
                .long("erase")
                .use_delimiter(true)
                .value_name("ids delimited by,")
                .multiple(true)
                .help("Paths to completely erase -- be careful")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("restore")
                .long("restore")
                .use_delimiter(true)
                .value_name("ids delimited by,")
                .help("Backups to restore")
                .multiple(true)
                .takes_value(true),
        )
        .get_matches()
}
