use super::error::Result;

use termion::input::TermRead;

use std::{sync::mpsc, thread};

pub enum Ev {
    Key(termion::event::Key),
    Tick,
}

#[derive(Debug)]
pub struct Events {
    rx: mpsc::Receiver<Ev>,
    _inp_t: thread::JoinHandle<()>,
    //_tick_t: thread::JoinHandle<()>,
}

impl Default for Events {
    fn default() -> Self {
        Events::new()
    }
}

impl Events {
    pub fn new() -> Self {
        let (tx, rx) = mpsc::channel();

        let _inp_t = {
            //let tx = tx.clone();
            thread::spawn(move || {
                let stdin = std::io::stdin();
                for ev in stdin.keys().flatten() {
                    if let Err(e) = tx.send(Ev::Key(ev)) {
                        error!("ev key sender {:?}", e);
                        return;
                    }
                }
            })
        };

        //// TODO Why did i add this here?
        // let _tick_t = thread::spawn(move || loop {
        //     if let Err(e) = tx.send(Ev::Tick) {
        //         error!("ev tick failed {:?}", e);
        //         return;
        //     }

        //     std::thread::sleep(Duration::from_millis(200));
        // });

        Events {
            rx,
            _inp_t,
            //_tick_t,
        }
    }

    pub fn next(&self) -> Result<Ev> {
        Ok(self.rx.recv()?)
    }
}
