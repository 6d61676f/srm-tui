pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Io(std::io::Error),
    MpscRecv(std::sync::mpsc::RecvError),
    TimeFmt(time::error::Format),
    TimeInvalid(time::error::InvalidFormatDescription),
    Backend(srm_core::error::Error),
    Log(log::SetLoggerError),
    SerdeJson(serde_json::Error),
    SerdeYaml(serde_yaml::Error),
    Missing,
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::SerdeJson(e)
    }
}

impl From<serde_yaml::Error> for Error {
    fn from(e: serde_yaml::Error) -> Self {
        Error::SerdeYaml(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::Io(e)
    }
}

impl From<std::sync::mpsc::RecvError> for Error {
    fn from(e: std::sync::mpsc::RecvError) -> Self {
        Error::MpscRecv(e)
    }
}

impl From<time::error::Format> for Error {
    fn from(e: time::error::Format) -> Self {
        Error::TimeFmt(e)
    }
}

impl From<time::error::InvalidFormatDescription> for Error {
    fn from(e: time::error::InvalidFormatDescription) -> Self {
        Error::TimeInvalid(e)
    }
}

impl From<srm_core::error::Error> for Error {
    fn from(e: srm_core::error::Error) -> Self {
        Error::Backend(e)
    }
}

impl From<log::SetLoggerError> for Error {
    fn from(e: log::SetLoggerError) -> Self {
        Error::Log(e)
    }
}
