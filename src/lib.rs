#[macro_use]
extern crate log;

extern crate comfy_table;
extern crate directories;
extern crate dirs;
extern crate srm_core;
extern crate serde_json;
extern crate serde_yaml;
extern crate simplelog;
extern crate time;
extern crate file_rotate;

pub mod cli;
pub mod error;
pub mod events;

#[cfg(target_os = "linux")]
pub mod tui;
