use std::{
    collections::{BTreeMap, HashMap},
    path::PathBuf,
};

use time::{format_description, OffsetDateTime};

use crate::error::Error;

use super::{
    error::Result,
    events::{Ev, Events},
};

use srm_core::{Id, SafeRm};
use termion::{event::Key, input::MouseTerminal, raw::IntoRawMode, screen::IntoAlternateScreen};
use tui::{
    backend::CrosstermBackend,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, Borders, Clear, List, ListItem, ListState, Paragraph, Tabs},
    Terminal,
};
/*
+-------------------------------------------------------------+--------------------------------------------------------------+
|                                                             |                                                              |
|                          Explorer                           |                         Thrash                               |
+-------------------------------------------------------------+--------------------------------------------------------------+
|                                                                                                                            |
|                                                                                                                            |
|                                                                                                                            |
|       Files from either thrash or CWD                                                                                      |
|                                                                                                                            |
|                                                                                                                            |
|                                                                                                                            |
+----------------------------------------------------------------------------------------------------------------------------+
|    Help bar with Quit, Restore, Erase, Backup, Navigation tips, Currently marked items                                     |
+----------------------------------------------------------------------------------------------------------------------------+
 */

#[derive(PartialEq, Copy, Clone)]
enum Tab {
    Thrash = 0,
    Files,
}

impl Tab {
    pub fn next(&mut self) {
        match self {
            Tab::Files => *self = Tab::Thrash,
            Tab::Thrash => *self = Tab::Files,
        }
    }

    pub fn prev(&mut self) {
        self.next()
    }
}

struct Tui<'a, T, E, S> {
    tab: Vec<&'a T>, // this will have the tabs // thrashed, explorer. from thrased we can choose erase or restore. explorer is file explorer
    // from which we can mark files for backup or erase
    tab_idx: Tab, // in what tab are we? Thrash or Explorer

    items_ctx: S, // used only in explorer mode for CWD

    items: BTreeMap<E, bool>, // items and if they are marked
    filter_selected: bool,    // if this is set we display only toggled
    items_ids: HashMap<E, Id>,
    list_state: ListState, // marked items

    events: Events, // in order to fetch the pressed keys

    erase_confirm: bool, // should display pop-up?

    // populate fields and stuff
    remover: SafeRm,
}

impl Tui<'static, &str, String, PathBuf> {
    pub fn new() -> Self {
        let mut tui = Tui {
            tab: vec![&"Thrash", &"Explorer"],
            tab_idx: Tab::Thrash,
            items_ctx: PathBuf::new(),
            items: BTreeMap::new(),
            filter_selected: false,
            items_ids: HashMap::new(),
            list_state: ListState::default(),
            events: Events::new(),
            erase_confirm: false,
            remover: SafeRm::new(),
        };

        tui.refresh_items();
        tui
    }

    fn refresh_thrash(&mut self) -> Result<()> {
        let metas = self.remover.list_backups()?;
        self.items.clear();
        self.items_ids.clear();
        for meta in metas {
            let fmt_dsc = format_description::parse(
                "[hour]:[minute]:[second]:[subsecond] [day]/[month]/[year]",
            )?;
            let dt: OffsetDateTime = meta.id.del_time.into();
            let item = format!(
                "{:30} {}",
                dt.format(&fmt_dsc)?,
                meta.id.orig_path.to_string_lossy()
            );

            self.items.insert(item.clone(), false);
            self.items_ids.insert(item, meta.id.clone());
        }

        Ok(())
    }

    fn refresh_dir(&mut self) -> Result<()> {
        if self.items_ctx.to_string_lossy().is_empty() {
            self.items_ctx =
                std::env::current_dir().or_else(|_| dirs::home_dir().ok_or(Error::Missing))?;
        }

        self.items.clear();

        for dir in std::fs::read_dir(&self.items_ctx)?.filter_map(|d| d.ok()) {
            let mut p: String = dir.file_name().to_string_lossy().into();
            let is_dir = dir.file_type().map(|k| k.is_dir()).unwrap_or(false);
            if is_dir {
                p.push('/');
            }
            self.items.insert(p, false);
        }

        //// NOTE
        //self.items.insert(String::from("../"), false);

        Ok(())
    }

    // called when tab is changed or newly initialized
    fn refresh_items(&mut self) {
        if self.tab_idx == Tab::Thrash {
            if let Err(e) = self.refresh_thrash() {
                error!("thrash refresh failed {:?}", e);
            }
        } else if let Err(e) = self.refresh_dir() {
            error!("dir refresh failed {:?}", e);
        }

        self.first_path();
    }

    pub fn get_window_partitioning(&self) -> Layout {
        Layout::default()
            .direction(Direction::Vertical)
            .constraints([
                Constraint::Length(3),
                Constraint::Min(0),
                Constraint::Length(3),
            ])
    }

    fn get_tab_titles(&self) -> Vec<Spans> {
        self.tab
            .iter()
            .map(|t| {
                let (first, other) = t.split_at(1);
                // we do this to have the first letter with another color...
                Spans::from(vec![
                    Span::styled(first, Style::default().fg(Color::Yellow)),
                    Span::styled(other, Style::default().fg(Color::Gray)),
                ])
            })
            .collect()
    }

    pub fn get_title_tab(&self) -> Tabs {
        Tabs::new(self.get_tab_titles())
            .block(Block::default().borders(Borders::ALL).title("Tabs"))
            .select(self.tab_idx as usize)
            .style(Style::default().fg(Color::Cyan))
            .highlight_style(Style::default().add_modifier(Modifier::BOLD))
    }

    pub fn get_outer_block(&self) -> Block {
        Block::default().style(Style::default())
    }

    fn are_central_items_selected(&self) -> bool {
        self.items
            .iter()
            .filter(|(_, val)| if self.filter_selected { **val } else { true })
            .count()
            != 0
    }

    fn get_central_list_items(&self) -> Vec<ListItem> {
        self.items
            .iter()
            .filter(|(_, val)| if self.filter_selected { **val } else { true })
            .map(|(key, val)| {
                let style = if *val {
                    Style::default().add_modifier(Modifier::UNDERLINED)
                } else {
                    Style::default()
                };

                ListItem::new(Spans::from(Span::styled(key, style)))
            })
            .collect()
    }

    fn get_erase_popup(&self) -> Paragraph {
        let style = Style::default().fg(Color::Cyan);
        let title = "Are you sure you want to erase the selected items?";

        let block = Block::default()
            .borders(Borders::ALL)
            .style(style)
            .title(title);

        Paragraph::new("Y/n")
            .alignment(Alignment::Center)
            .style(Style::default().fg(Color::Red))
            .block(block)
    }

    pub fn get_central_list(&self) -> List {
        let style = Style::default().fg(Color::Yellow);

        let title = if self.tab_idx == Tab::Thrash {
            String::new()
        } else {
            self.items_ctx.to_string_lossy().to_string()
        };

        List::new(self.get_central_list_items())
            .block(Block::default().borders(Borders::ALL).title(title))
            .style(style)
            .highlight_style(Style::default().add_modifier(Modifier::BOLD))
            .highlight_symbol(">> ")
    }

    fn get_help_list_items(&self) -> Vec<Spans> {
        let tab = "Tab to switch mode";
        let arrows = "✢";
        let arrows2 = "←→";
        let spc = "Space to mark item";
        let list = "List selected items";
        let toggle = "*/0/1 to toggle/unselect/select all";

        let helps = if self.tab_idx == Tab::Thrash {
            vec![arrows2, spc, "Restore", "Erase", list, tab, toggle]
        } else {
            vec![arrows, spc, "Backup", list, tab, toggle]
        };

        helps
            .iter()
            .map(|help| {
                let help: Vec<char> = help.chars().collect();
                let (first, rest) = help.split_at(1);
                Spans::from(vec![
                    Span::styled(
                        first.iter().collect::<String>(),
                        Style::default().fg(Color::Red),
                    ),
                    Span::styled(
                        rest.iter().collect::<String>(),
                        Style::default().fg(Color::Gray),
                    ),
                ])
            })
            .collect()
    }

    pub fn get_help(&self) -> Tabs {
        let helps = self.get_help_list_items();

        Tabs::new(helps)
            .block(Block::default().borders(Borders::ALL).title("Commands"))
            .style(Style::default().fg(Color::Cyan))
            .divider(" ")
    }

    pub fn get_event(&self) -> Result<Ev> {
        self.events.next()
    }

    pub fn next_tab(&mut self) {
        if self.erase_confirm {
            return;
        }
        self.tab_idx.next();
        self.refresh_items();
    }

    pub fn prev_tab(&mut self) {
        if self.erase_confirm {
            return;
        }

        self.tab_idx.prev();
        self.refresh_items();
    }

    pub fn up_dir(&mut self) {
        if self.erase_confirm {
            return;
        }

        if let Some(parent) = self
            .items_ctx
            .parent()
            .map(|parent| parent.to_path_buf())
            .and_then(|parent| parent.canonicalize().ok())
        {
            self.items_ctx = parent;
            self.refresh_items();
        }
    }

    pub fn down_dir(&mut self) {
        if self.erase_confirm {
            return;
        }

        if let Some(newcwd) = self
            .list_state
            .selected()
            .and_then(|cidx| self.items.iter().nth(cidx))
            .and_then(|(elem, _)| {
                let mut newcwd = self.items_ctx.clone();
                newcwd.push(elem);

                newcwd.canonicalize().ok()
            })
            .filter(|newcwd| newcwd.is_dir())
        {
            self.items_ctx = newcwd;
            self.refresh_items();
        }
    }

    fn first_path(&mut self) {
        self.list_state.select(Some(0));
    }

    fn get_items_len(&self) -> usize {
        let len = if self.filter_selected {
            let mut count = 0;
            self.items.iter().for_each(|(_, v)| {
                if *v {
                    count += 1
                }
            });
            count
        } else {
            self.items.len()
        };

        if len != 0 {
            len - 1
        } else {
            len
        }
    }

    fn next_path(&mut self) {
        let len = self.get_items_len();

        let i = self
            .list_state
            .selected()
            .map(|i| if i >= len { 0 } else { i + 1 });

        self.list_state.select(i);
    }

    fn prev_path(&mut self) {
        let len = self.get_items_len();

        let i = self
            .list_state
            .selected()
            .map(|i| if i == 0 { len } else { i - 1 });

        self.list_state.select(i);
    }

    pub fn handle_left(&mut self) {
        if self.tab_idx == Tab::Files {
            self.up_dir()
        }
    }

    pub fn handle_right(&mut self) {
        if self.tab_idx == Tab::Files {
            self.down_dir()
        }
    }

    pub fn handle_up(&mut self) {
        if self.erase_confirm {
            return;
        }

        self.next_path()
    }
    pub fn handle_down(&mut self) {
        if self.erase_confirm {
            return;
        }

        self.prev_path()
    }

    pub fn toggle(&mut self) {
        if self.erase_confirm {
            return;
        }

        // this works, but god if i understand it...
        if let Some(mut idx) = self.list_state.selected() {
            for (i, (_, elem)) in self.items.iter_mut().enumerate() {
                if self.filter_selected && !*elem {
                    idx += 1;
                } else if idx == i {
                    *elem ^= true;
                    break;
                }
            }
            self.handle_up();
        }
    }

    pub fn toggle_all(&mut self) {
        if self.erase_confirm {
            return;
        }

        self.items.iter_mut().for_each(|(_, v)| *v ^= true);
    }

    pub fn select_all(&mut self) {
        if self.erase_confirm {
            return;
        }

        self.items.iter_mut().for_each(|(_, v)| *v = true);
    }
    pub fn unselect_all(&mut self) {
        if self.erase_confirm {
            return;
        }

        self.items.iter_mut().for_each(|(_, v)| *v = false);
    }

    pub fn confirm_erase(&mut self) {
        if self.tab_idx == Tab::Files {
            return;
        }

        let filter = self.filter_selected;
        self.filter_selected = true;

        if !self.are_central_items_selected() {
            self.filter_selected = filter;
            return;
        }

        self.erase_confirm = true;
    }

    // TODO
    // do not permit deleting of '..' -- we don't yield it as a dir
    pub fn backup(&mut self) {
        if self.tab_idx == Tab::Thrash {
            return;
        }

        let paths: Vec<PathBuf> = self
            .items
            .iter()
            .filter(|(_, v)| **v)
            .map(|(k, _)| self.items_ctx.join(k))
            .collect();

        debug!("backing up {:?}", paths);
        self.remover
            .backup(&paths)
            .into_iter()
            .zip(paths)
            .for_each(|(err, path)| match err {
                Ok(_) => debug!("succesfully backed up {}", path.to_string_lossy()),
                Err(e) => error!("failed backing up {} {:?}", path.to_string_lossy(), e),
            });

        self.refresh_items();
    }

    pub fn erase(&mut self) {
        if self.tab_idx == Tab::Files || !self.erase_confirm {
            return;
        }

        let ids: Vec<Id> = self
            .items
            .iter()
            .filter(|(_, v)| **v)
            .map(|(k, _)| self.items_ids.get(k).ok_or(Error::Missing))
            .filter_map(|id| id.ok().cloned())
            .collect();

        debug!("erasing {:?}", ids);
        self.remover
            .erase_by_id(&ids)
            .into_iter()
            .zip(ids)
            .for_each(|(err, id)| match err {
                Ok(_) => debug!("succesfully erased {}", id.get_handle()),
                Err(e) => error!("failed erasing {:?} {:?}", id, e),
            });

        self.quit_popup();
        self.refresh_items()
    }

    pub fn restore(&mut self) {
        if self.tab_idx == Tab::Files || self.erase_confirm {
            return;
        }

        let ids: Vec<Id> = self
            .items
            .iter()
            .filter(|(_, v)| **v)
            .map(|(k, _)| self.items_ids.get(k).ok_or(Error::Missing))
            .filter_map(|id| id.ok().cloned())
            .collect();

        debug!("restoring {:?}", ids);

        self.remover
            .restore_by_id(&ids)
            .into_iter()
            .zip(ids)
            .for_each(|(err, id)| match err {
                Ok(_) => debug!("succesfully restored {}", id.get_handle()),
                Err(e) => error!("failed restoring {:?} {:?}", id, e),
            });

        self.refresh_items();
    }

    pub fn quit_popup(&mut self) {
        if self.erase_confirm {
            self.filter_selected = false;
            self.erase_confirm = false;
        }
    }

    pub fn display_selection(&mut self) {
        if self.erase_confirm {
            return;
        }

        self.filter_selected ^= true;
        self.first_path();
    }
}

fn popup(r: Rect) -> Rect {
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage(33),
                Constraint::Length(3),
                Constraint::Percentage(66),
            ]
            .as_ref(),
        )
        .split(r);

    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage(25),
                Constraint::Percentage(50),
                Constraint::Percentage(25),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}

pub fn run() -> Result<()> {
    let sout = std::io::stdout().into_raw_mode()?;
    let sout = MouseTerminal::from(sout).into_alternate_screen()?;
    let backend = CrosstermBackend::new(sout);
    let mut term = Terminal::new(backend)?;

    let mut tui = Tui::new();

    loop {
        term.draw(|f| {
            let sz = f.size();

            //Draw outer block
            let block = tui.get_outer_block();
            f.render_widget(block, sz);

            // get the tree parts of the screen
            // TODO we should split this so that each rectangle is a sub
            // layout of itself with custom options
            let chunks = tui.get_window_partitioning().split(sz);

            // draw the title tab
            let top = tui.get_title_tab();
            f.render_widget(top, chunks[0]);

            // draw the list of items
            let middle = tui.get_central_list();

            let mut state = tui.list_state.clone();
            f.render_stateful_widget(middle, chunks[1], &mut state);
            tui.list_state = state;

            //draw pop-up confirmation for erase if applicable
            if tui.erase_confirm {
                let erase_popup = tui.get_erase_popup();
                let area = popup(sz);
                f.render_widget(Clear, area); //this clears out the background
                f.render_widget(erase_popup, area);
            }

            // draw help list
            let bottom = tui.get_help();
            f.render_widget(bottom, chunks[2]);
        })?;

        if let Ev::Key(k) = tui.get_event()? {
            match k {
                Key::Char('q') => break,
                Key::Char('\t') => tui.next_tab(),
                Key::BackTab => tui.prev_tab(),
                Key::Char('l') => tui.display_selection(),
                Key::Char('b') => tui.backup(),
                Key::Char('e') => tui.confirm_erase(),
                Key::Char('Y') => tui.erase(),
                Key::Char('n') => tui.quit_popup(),
                Key::Char('r') => tui.restore(),
                Key::Char(' ') => tui.toggle(),
                Key::Char('*') => tui.toggle_all(),
                Key::Char('0') => tui.unselect_all(),
                Key::Char('1') => tui.select_all(),
                Key::Left => tui.handle_left(),
                Key::Right => tui.handle_right(),
                Key::Down => tui.handle_up(),
                Key::Up => tui.handle_down(),
                _ => (),
            }
        }
    }

    Ok(())
}
