use file_rotate::{compression::Compression, suffix::AppendCount, ContentLimit, FileRotate};
use log::LevelFilter;
use simplelog::{Config, WriteLogger};
use srm_ui::{cli, error::Result, tui};

fn main() -> Result<()> {
    if let Some(bdirs) = directories::ProjectDirs::from("com", "meh", env!("CARGO_PKG_NAME")) {
        let mut cache_lfile = bdirs.cache_dir().to_path_buf();
        cache_lfile.push("log");

        let f = FileRotate::new(
            cache_lfile,
            AppendCount::new(10),
            ContentLimit::Lines(1024),
            Compression::None,
            #[cfg(unix)]
            None,
        );

        WriteLogger::init(LevelFilter::Debug, Config::default(), f)?;
    }

    let args = cli::get_args();
    if cfg!(target_os = "linux") && args.get_flag("tui") {
        tui::run()?;
    } else {
        cli::run(args)?;
    }

    Ok(())
}
