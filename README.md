## Saferm Terminal User Interface

### Leverage the saferm backend in order to create an in-place alternative to `rm`

#### _What_ it should do

- Four modes (at least)
    - remove
        - this should follow the options of POSIX `rm` and backup given files
    - list
        - list all the backups in either TUI mode or Json/Yaml/whatever
    - restore
        - interactive TUI table mode that lets you select files and then confirms
         their restoration
    - erase
        - interactive TUI table mode that lets you select what backup files
         are to be removed for good
- give the option to start in TUI or classic CLI mode
- log to file I suppose

#### _How_ it should do it

- use some library that wraps ncurses/whatever; currently tui-rs
- basically check the passed option to determine what mode to launch
    - these options should be in long format in order not to conflict with options from `rm`
    - maybe remove mode should be the default one
    - besides the mode we also need an interactive/TUI or programatic output flag
- for interactive mode we should add a lot of confirmations for the user
- i have to check what options i have for logging
    - i saw something regarding a logging widget
    - file to temporary filesystem otherwise?
